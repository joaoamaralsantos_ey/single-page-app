define(function(){

    function render(page, params){
        
        drawSearchInput();
        
        if(params){
            drawResults(params, page);
        }

    };

    function drawSearchInput(){

        $('#app').html('<div class="ui category search">' +
                       '<div class="ui icon input">' +
                       '<input class="prompt" placeholder="Search ..." type="text">' +
                       '<i class="search icon"></i>' +
                       '</div>' +
                       '</div>' +
                       '<div class="results" style="padding:40px"></div>');
    };

    function drawResults(params, page){

        if(params.length == 0 ){
           return $('.results').html(withoutResults())
        }

        $('.results').html(createCards(createCard(params))).append(pagination(page));
    };
    
    function createCards(arrCards){

        body = arrCards.reduce(function(acc, curVal){
            return acc + curVal;
        });

        return '<div class="ui grid container">' +
               '<div class="ui six stackable cards">' +
               body +
               '</div>' +
               '</div>'
    };

    function createCard(arrCompanies){

        allCards = [];

        arrCompanies.map(function(company, index){
            allCards.push(

                '<div class="card">' +
                '<div class="content">' +
                '<div class="header">' +
                  company.ticker +
                '</div>' +
                '<div class="meta">' +
                  company.name +
                '</div>' +
                '</div>' +
                '<div class="details ui bottom attached button" id=' + company.ticker + '>' +
                '<i class="add icon"></i>' +
                '</div>' +
                '</div>'
            )
        });

        return allCards;
    }

    function withoutResults(){
        return '<div class= "ui compact segment" style="margin: auto; background-color:#1b1c1d; border:none;">' +
               '<div class="ui yellow message">' +
               '<div class="header">' +
               'Without results' +
               '<p>Try another company</p>' +
               '</div></div></div>';
    }

    function pagination(page){
        return '<div class="ui buttons" style="padding: 50px">' +
               '<button id="previous" class="ui labeled icon button">' +
               '<i class="left chevron icon"></i>' +
               'Previous' +
               '</button>' +
               '<button class="ui button">' +
               page +
               '</button>' +
               '<button id="next" class="ui right labeled icon button" style="white-space: pre;">' +
               '   Next   ' +
               '<i class="right chevron icon"></i>' +
               '</button>' +
               '</div>'
    }

    function bindSearchHandler(handler){

        $('.prompt').on('change', handler);
    };

    function bindCompanyHandler(handler){

        $('.details').on('click', handler);
    };
    
    function bindPageHandler(handler){
        
        $('.labeled').on('click', handler);
    };


    return {
        render: render,
        bindSearchHandler: bindSearchHandler,
        bindCompanyHandler: bindCompanyHandler,
        bindPageHandler: bindPageHandler
    };
})