define([
    'controllers/details-controller',
], function(detailsController) {

    var ticker;

    function render(data){

        console.log(data)

        $('#app').html(listItem(createItems(data[0]), ticker)).append(createStockPriceCard(data[1]));
    };

    function renderError(objectError){
        console.log(objectError)
        $('#app').html(error(objectError));
    }
    
    function listItem(data, ticket){

        list = data.reduce(function(acc, curVal){
            
            return acc + curVal;
        });
        
        return '<div class="ui centered card">' +
               '<div class="content"><div class="header">'+ ticker + '</div>' +
               '<div class="extra content left aligned">' +
               '<ul style="padding-left: 0px;">' + list + '</ul>' +
               '</div>' +
               '</div>';
    };
    
    function createItems(data){
        
        ticker = data.ticker;
        
        return Object.keys(data).map(function(key){
            return '<h4 class="ui sub header">'+ key + '</h4>' +
            '<div class="ui small feed">' + 
            '<div class="summary">' + 
            '<p style="color: black">' + data[key] + '<p>'+
            '</div>' + 
            '</div>' + 
            '</h4>' 
        })
    };

    function createStockPriceCard(data){

        if(!data){
            return createLabel(null, 'yellow')
        }
        else if(data.delta[0] === '-'){
            return createLabel(data, 'red')
        }
        else{
            return createLabel(data, 'green')
        }
    }

    function createLabel(object, color){

        if(!object){
            number = '-';
        }
        else{
            number = object.delta;
        }

       return '<div class="ui centered card">' + 
              '<div class="ui ' + color + ' statistic">' +
              '<div class="value">' +
              number +
              '</div>' +
              '<div class="label">' +
              'Last day stock change' +
              '</div>' +
              '</div>'
    }
    
    function error(objectError){
        errorText = objectError.status + ' ' + objectError.statusText

        return '<div class= "ui compact segment" style="margin: auto; background-color:#1b1c1d; border:none;">' +
               '<div class="ui negative message">' +
               '<div class="header">' +
               'Something went wrong' +
               '<p>' + errorText + '</p>' +
               '</div></div></div>';
    }
    return{
        render: render,
        renderError: renderError
    }
});