define(
 function() {

    function CompanyStockPrice(ticker, date, delta){

        this.ticker = ticker;
        this.date = date;
        this.delta = delta;
    }

    return CompanyStockPrice;
}); 