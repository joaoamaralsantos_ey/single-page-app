define(
 function() {

    function CompanyDetails(ticker, name, shortDescription, ceo, companyUrl, businessAddress){

        this.ticker = ticker;
        this.name = name;
        this.shortDescription = shortDescription;
        this.ceo = ceo;
        this.companyUrl = companyUrl;
        this.businessAddress = businessAddress;
    }

    return CompanyDetails;
}); 