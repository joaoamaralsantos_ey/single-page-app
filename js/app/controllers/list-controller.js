define([
    'views/list-view',
    'services/company-service'
], function(listView, companyService){

    var page = 1;
    var allResults;
    var nrOfPages;
    var lastSearch;

    function start(){
        
        if(allResults){
            return renderList(page, paginate(page));
        }
        
        listView.render()

        listView.bindSearchHandler(searchHandler);
    };

    function renderList(data){

        filterData = paginate(page)

        listView.render(page, filterData);
        listView.bindSearchHandler(searchHandler);
        listView.bindCompanyHandler(companyHandler);
        listView.bindPageHandler(pageHandler);
    };

    function paginate(page){

        resultsPerPage = 12;
        end = page * resultsPerPage;
        begin = end - resultsPerPage;

        return allResults.slice(begin, end)

    }

    function searchHandler(event){

        queryParams = event.target.value;
        lastSearch = queryParams;
        page = 1;

        companyService.searchData(queryParams).then(function(results){

            allResults = results;
            nrOfPages = Math.ceil(allResults.length/12)
            renderList(results);
        });
    };

    function companyHandler(event){

        ticker = event.currentTarget.id;

        window.location.hash = '#details';
        companyService.setTicker(ticker);
    };

    function pageHandler(event){

        if(page < nrOfPages && event.target.id == 'next'){
            page++;
            renderList();
        }
        if(page > 1 && event.target.id == 'previous'){
            page--;
            renderList();
        }
    }
    
    return {
        start: start
    };
})