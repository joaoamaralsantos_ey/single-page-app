define([
    'services/company-service',
    'views/details-view'
], function(companyService, detailsView) {

    function start(){

       Promise.all([
           companyService.getDetails(),
           companyService.getStockPrice(ticker)])
           .then(render, renderError);

    };

    function render(obj){
        detailsView.render(obj);
    };

    function renderError(error){
        detailsView.renderError(error);
    };

    return{
        start: start
    };
}); 