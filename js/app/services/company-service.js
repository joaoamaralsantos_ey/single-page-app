define([
    'models/company',
    'models/company-details',
    'models/company-stock-price'
], function(Company, CompanyDetails, CompanyStockPrice){
    
    var currentTicker;

    function b64EncodeUnicode(str) {
        return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
            function toSolidBytes(match, p1) {
                return String.fromCharCode('0x' + p1);
            }));
    };
    
    function authenticate(){
        var username = 'fd35eb0ea51536d2cc1c2d9e32c2a540';
        var password = '576e13f039c883b4166bedb225a2a954';
        var auth = "Basic " + b64EncodeUnicode(username + ':' + password);
 
        return auth;
    };

    function apiRequestIntrinio(path, cb){
        return new Promise(function(resolve, reject){
            
            $.ajax({
                headers: {
                    'Authorization': authenticate()
                },
                url: 'https://api.intrinio.com' + path,
                type: 'GET',
                dataType: 'json',
                success: resolve,
                error: reject
            });
        })
    };

    function apiRequestAlphaVantage(ticker, cb){

        var apiKey = 'M8X1P8HRL3AHWBQP'

        $.ajax({
            url: 'https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=' + ticker + '&interval=1min&apikey=' + apiKey,
            type: 'GET',
            dataType: 'json',
            success: function(results){
                cb(null, results)
            },
            error: function(request) {
                cb(request.responseText)
            }
        });
    };
    
    function getDetails(){ 
        
        var path = '/companies?ticker=' + currentTicker;
        
        var companyDetails;
        
        return apiRequestIntrinio(path).then(function(results){
            
            return companyDetails = new CompanyDetails(results.ticker, results.name, results.short_description, results.ceo, results.company_url, results.business_address)
        }, function(results){
            
            throw results;
        })
    };
    
    function getStockPrice(ticker){
        
        var date = '2018-02-20';
        
        var path = '/prices?identifier=' + ticker + '&start_date=' + date + '&end_date=' + date + '&frequency=daily';

        return apiRequestIntrinio(path).then(function(results){

            if(!results.data[0]){
                return;
            }

            data_results = results.data[0]
            delta = ((data_results.open - data_results.close)/100).toLocaleString('de-DE', { style:'percent', maximumFractionDigits: 3 } )

            return companyStockPrice = new CompanyStockPrice(ticker, data_results.date, delta)
            
        })
    };
    
    function searchData(queryParams){
        
        var path = '/companies?query=' + queryParams;
        
        return apiRequestIntrinio(path).then(function(results){
            
            return  allCompanies = results.data.map(function(company){
                newCompany = new Company(company.ticker, company.name);
                return newCompany;
            })
        })
    };

    function getTicker(){
        return currentTicker;
    };
 
    function setTicker(ticker){
 
         currentTicker = ticker;
    };

    function getDate(){
        var today = new Date();
        var yesterday = today.setDate(today.getDate() - 1);
        console.log(yesterday)
        var weekDay = yesterday.getDay();

        switch(weekDay){
            case 0:
                yesterday.setDate(yesterday.getDate() - 2);
            case 6:
                yesterday.setDate(yesterday.getDate() - 1 );
        }

        var year = yesterday.getFullYear();
        var month = yesterday.getMonth() + 1;
        var day = yesterday.getDate()

        if(day < 10){
            day = '0' + day;
        }

        if(month < 10){
            month = '0' + month;
        }
        
        return year + '-' + month + '-' + day;
    }
    
    return {
        searchData: searchData,
        getDetails: getDetails,
        setTicker: setTicker,
        getTicker: getTicker,
        getStockPrice: getStockPrice
  }
    
});